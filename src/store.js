import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todos: []
  },
  mutations: {
    add: (state, payload) => {
      state.todos.push({ title: payload })
    },
    remove: (state, payload) => {
      state.todos.splice(payload, 1)
    },
    update: (state, payload) => {
      state.todos[payload.id] = { title: payload.payload }
    }
  },
  actions: {

  }
})
